require 'sinatra'
require_relative 'models/faq'

configure do
  enable :sessions
  set :session_secret, 'b0sco'
  disable :protection
  if ENV['RACK_ENV'] != "development"
    set :static_cache_control, [:public, :max_age => 31536000]
  end
end

before do
  @serve_minified = false
  if ENV['RACK_ENV'] != "development"
    expires 3000, :public, :must_revalidate
    @serve_minified = true
  end
end

get '/' do
  erb :index, :layout => :layout
end

get '/our-company' do
  erb :our_company, :layout => :layout
end

get '/our-products' do
  erb :our_products, :layout => :layout
end

get '/our-products/list' do
  erb :products_listing, :layout => :layout
end

get '/our-products/detail' do
  erb :products_detail, :layout => :layout
end

get '/our-caring' do
  erb :our_caring, :layout => :layout
end

get '/search' do
  erb :search_results, :layout => :layout
end

get '/parts' do
  erb :parts, :layout => :layout
end

get '/faqs' do
  erb :faq, :layout => :layout
end

#get '/faqs/detail' do
#  erb :faq_detail, :layout => :layout
#end

get '/dealers' do
  erb :dealers_search, :layout => :layout
end

get '/dealers/results' do
  erb :dealers_results, :layout => :layout
end

get '/dealers/directions' do
  erb :dealers_directions, :layout => :layout
end

get '/dealers/directions/results' do
  erb :dealers_directions_results, :layout => :layout
end

get '/product-registration' do
  erb :product_registration, :layout => :layout
end

get '/product-registration/survey' do
  erb :product_registration_survey, :layout => :layout
end

get '/BuildFAQFeedView' do
  FAQ.get_faqs(params['feedAlias'])
end
