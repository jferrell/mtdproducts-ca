function getAjaxFaqFeed(storeId, catalogId, langId, feedAlias, feedMax){
	$("#feedFaqDivId").html(document.MessageForm.pleaseWaitMsg.value);
	$.ajax({
		url: '/BuildFAQFeedView?langId=' + langId + '&storeId=' + storeId + '&catalogId=' + catalogId + '&feedAlias=' + feedAlias + '&feedMax=' + feedMax,
		type: 'GET', // GET or POST
		dataType: 'html', //return data type
		cache: false,
		timeout: 6000, //might need to bump up in dev environment
		error: function(){
			$("#feedFaqDivId").html(document.MessageForm.loadFailureMsg.value);
		},
		success: function(html){
			$("#feedFaqDivId").html(html);
		}
	});
}

$(function () {
	$('.faq .trigger a').click(function () {
		if(!$(this).hasClass('active')) {
			$('.faq .trigger a').removeClass('active');
			$(this).addClass('active');
			$('.faq .trigger .tab_content').hide();
			$(this).next('.tab_content').show();
		}
	});
});
