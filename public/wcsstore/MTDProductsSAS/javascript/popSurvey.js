// JavaScript Document
// 2010 Marcus Thomas LLC - Nick Zwinggi - Web Developer
// NOTES SECTION - START
//
// Line 28 - "randomnumber < 90", 90 is the percentage the popup will be displayed. 
// Line 29,30 - the number at the end is the milliseconds that the popup will be delayed. Currently set for 6500 (6.5 seconds) and 6000 (6 seconds)
// Line 42 - The link to the actual survey
//
// NOTES SECTION - END

/*

document.write('<div id="surveyBG"></div>');
document.write('<div id="survey1">');
document.write('<p class="title">Thank you for visiting our site!</p>');
document.write('<p>Upon leaving our website, you may be asked to take part in a website survey. The survey should take less than five minutes to complete.</p>');
document.write('<p class="bottom">All results are strictly confidential. Are you willing to help?</p>');
document.write('<a href="#" onclick="setCookie(&#39;mtd&#39;,&#39;surveyYes&#39;,10000)" class="yes">Yes</a>');
document.write('<a href="#" onclick="setCookieNo(&#39;mtd&#39;,&#39;surveyNo&#39;)">No Thanks</a>');
document.write('</div>');

mtd=getCookie('mtd');
if (mtd!=null && mtd!="")
  {
  }
else
  {
	var randomnumber=Math.floor(Math.random()*101)

	if (randomnumber < 90) {
		var t=setTimeout("document.getElementById('survey1').style.display = 'block';",6500);
		var t=setTimeout("document.getElementById('surveyBG').style.display = 'block';",6000);
	}
  }



function setCookie(c_name,value,expiredays)
{
var exdate=new Date();
exdate.setDate(exdate.getDate()+expiredays);
document.cookie=c_name+ "=" +escape(value)+
((expiredays==null) ? "" : ";expires="+exdate.toUTCString());
window.open('https://www.surveymonkey.com/s/universal2013','','scrollbars=yes,width=660,height=550,left=100,top=100').blur();
document.getElementById('survey1').style.display = 'none'
document.getElementById('surveyBG').style.display = 'none'
}function setCookieNo(c_name,value,expiredays)
{
var exdate=new Date();
exdate.setDate(exdate.getDate()+expiredays);
document.cookie=c_name+ "=" +escape(value)+
((expiredays==null) ? "" : ";expires="+exdate.toUTCString());
document.getElementById('survey1').style.display = 'none'
document.getElementById('surveyBG').style.display = 'none'
}



function getCookie(c_name)
{
if (document.cookie.length>0)
  {
  c_start=document.cookie.indexOf(c_name + "=");
  if (c_start!=-1)
    {
    c_start=c_start + c_name.length+1;
    c_end=document.cookie.indexOf(";",c_start);
    if (c_end==-1) c_end=document.cookie.length;
    return unescape(document.cookie.substring(c_start,c_end));
    }
  }
return "";
}

*/