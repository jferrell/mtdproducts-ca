$(function() {	
	// OUR COMPANY -> Our Heritage
	// thumb click disable
	$(".year .thumb").click(function(e) {
		e.preventDefault();
	});
	$(".year .thumb").hover(
		function() {
			var container = $(this).parent().parent().parent();
			var description_area = $(".tl_description", container);
			var large_photo = $(".large_photo", $(this));
			if (! large_photo.hasClass("default"))
			{
				var copy = large_photo.html();
				description_area.prepend("<div class='img hovered'>"+copy+"</div>");
				$(".tl_description .default", container).hide();				
			}
		},
		function() {
			var container = $(this).parent().parent().parent();
			if (! $(".large_photo", $(this)).hasClass("default"))
			{
				$(".tl_description .hovered").addClass("large_photo")
											.removeClass("hovered");
				$(".tl_description .default", container).show();				
			}
		}
	);
	// prev + next click
	$(".year .browse").click(function(e) {
		// get the tabs
		var tabs = $(".tab");
		// get the current active thumb
		var active = $(".ui-state-active");
		var index = tabs.index(active);
		var tab_click = 0;

		// click prev/next tab
		if (index == 0)
		{
			if ($(this).hasClass("prev"))
			{
				tab_click = tabs.length-1;
			}else
			{
				tab_click = index+1;
			}
		}else
		{
			if ($(this).hasClass("prev"))
			{
				tab_click = index-1;
			}else
			{
				if (index == (tabs.length-1))
				{
					tab_click = 0;
				}else
				{
					tab_click = index+1;
				}
			}
		}
		// click the tab
		$(".tab a:eq("+(tab_click)+")").click();
	});
	

	
	// PRINT
	$(".print").click(function(e) {
		window.print();
	});
	// SEARCH -> tabs
	//$("#tabs").tabs();	
	// HOME PAGE -> Hero's Cycle
	$('#hero_features').before('<div id="hero_paging">').cycle({ 
	    speed:  'slow', 
	    timeout: 5000,
		pager: '#hero_paging' 
	});	
	// HOME PAGE -> Brand's Scrollable
	$("#brands .scrollable").scrollable({ circular: false });
	// HOME PAGE -> Retailer's Scrollable
	$("#retailers .scrollable").scrollable({ circular: false });
	

    function megaHoverOver(){
          $(this).find(".sub").stop().fadeTo('fast', 1).show();
          $(this).addClass("hovering");

          //Calculate width of all ul's
          (function($) { 
              jQuery.fn.calcSubWidth = function() {
                  rowWidth = 0;
                  //Calculate row
                  $(this).find("ul").each(function() {					
                      rowWidth += $(this).width(); 
                  });	
              };
			})(jQuery); 
          
          if ( $(this).find(".row").length > 0 ) { //If row exists...
              var biggestRow = 0;	
              //Calculate each row
              $(this).find(".row").each(function() {							   
                  $(this).calcSubWidth();
                  //Find biggest row
                  if(rowWidth > biggestRow) {
                      biggestRow = rowWidth;
                  }
              });
              //Set width
              $(this).find(".sub").css({'width' :biggestRow});
              $(this).find(".sub").css({'z-index' : 9999999});
              $(this).find(".row:last").css({'margin':'0'});
              
          } else { //If row does not exist...
              
              $(this).calcSubWidth();
              //Set Width
              $(this).find(".sub").css({'width' : rowWidth});
              $(this).find(".sub").css({'z-index' : 9999999});
              
          }
      }
      
      function megaHoverOut(){ 
        $(this).find(".sub").stop().fadeTo('fast', 0, function() {
            $(this).hide(); 
        });
		$(this).removeClass("hovering");
      }
  
      var config = {    
           sensitivity: 1, // number = sensitivity threshold (must be 1 or higher)    
           interval: 120, // number = milliseconds for onMouseOver polling interval    
           over: megaHoverOver, // function = onMouseOver callback (REQUIRED)    
           timeout: 100, // number = milliseconds delay before onMouseOut    
           out: megaHoverOut // function = onMouseOut callback (REQUIRED)    
      };
  
      $("ul#topnav li .sub, ul.globe li .sub").css({'opacity':'0'});
      $("ul#topnav li, ul.globe li").hoverIntent(config);


    $('.stars').each(function () {
    	var max_stars = 5;
			if($(this).find('em').size() > 0) {
				var max_width = parseInt($(this).css('width'));
				var star_rating = parseFloat($(this).find('em').html());
				var star_width = Math.round((star_rating / max_stars) * max_width);
				$(this).find('em').css('width',star_width + 'px');
			}
    });

    $('.swap-large-image').click(function () {
    	var target = $('#main-product-image');
    	var large_image = $(this).attr('href')
    	var link = $(this).attr('rel');
    	$(target).find('a').attr('href', link);
    	$(target).find('img').attr('src', large_image);
    	return false;
    });



    var canada = $('html.canada');
    if (canada.length > 0) {
      // product registration checkboxes
      var underFourteen = $('#under14');
      var underFourteenMsg = $('#under14_message');
      if (underFourteen.length > 0) {
        underFourteen.click(function() {
          underFourteenMsg.toggleClass('hide');
        });
      }
      var infoCollection = $('#info_collection');
      var infoCollectionMsg = $('#info_collection_message');
      if (infoCollection.length > 0) {
        infoCollection.click(function() {
          infoCollectionMsg.toggleClass('hide');         
        });
      }

      // feedfaq
      $('#feedFaqDivId a').live('click', function() {
        if(!$(this).hasClass('active')) {
          $(this).addClass('active');
        } else {
          $(this).removeClass('active');
        }
        return false
      });
    }

});