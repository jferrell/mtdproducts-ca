//cubMapquest.js
//functions and constants used to process maps for location services
var HOST_URL = 'http://www.mapquestapi.com'; 
var APP_KEY = 'Gmjtd%7Cluua2hubnl%2Cbn%3Do5-lz8l5';
var REVERSE_POST = HOST_URL + '/geocoding/v1/reverse?key=YOUR_KEY_HERE&json={location:{latLng:{lat:LAT,lng:LNG}}}&callback=renderGeocode';
var ADDRESS_POST = HOST_URL + '/geocoding/v1/address?key=YOUR_KEY_HERE&callback=updateGeocode&json={location:{street:"STREET"}}';
var SIDEBAR_POST = HOST_URL + '/geocoding/v1/address?key=YOUR_KEY_HERE&callback=updateSidebar&json={location:{street:"STREET"}}';
var CAN_PARTS_POST = HOST_URL + '/geocoding/v1/address?key=YOUR_KEY_HERE&callback=submitCanadaParts&json={location:{street:"STREET"}}';
var AJAX_POST = HOST_URL + '/geocoding/v1/address?key=YOUR_KEY_HERE&json={location:{street:"STREET"}}';
var strlat = '';
var strlng = '';
	


function initiate_geolocation() {  
    navigator.geolocation.getCurrentPosition(handle_geolocation_query); 
    
        }  
var form1 = document.dealerSearchForm;
function handle_geolocation_query(position){
    	var form1 = document.dealerSearchForm;
    	strlat = position.coords.latitude; 
        strlng = position.coords.longitude;
        form1.elements["latitude"].value  = position.coords.latitude;  
        form1.elements["longitude"].value  = position.coords.longitude;
        doClick();  
            
  }  
          
 function useLocation(){
	MQA.withModule('geocoder', function() {
		var form1 = document.dealerSearchForm;
		var latLng = {lat:strlat, lng:strlng};
		/*Executes a geocode with an object containing lat/lng properties, adds result to the map, and 
		adds a function to be called once geocoding is complete.*/ 
    map.reverseGeocodeAndAddLocation(
     latLng, doClick
    );

  /*Example function used to show the address of the geocoded location*/ 
  function showAddress(data) {

      var response = data.results[0].locations[0];
      var form1 = document.dealerSearchForm;
      
  if(form1.elements["origaddress"])
	  form1.elements["origaddress"].value= response.street;
  if(form1.elements["origcity"])
	  form1.elements["origcity"].value= response.adminArea5;
  if(form1.elements["origstateprovince"])
	  form1.elements["origstateprovince"].value= response.adminArea3;
  if(form1.elements["origPostal"])
	  form1.elements["origPostal"].value= response.postalCode;
  if(form1.elements["zipCode"])
	  form1.elements["zipCode"].value= response.postalCode;
      	
    }
  });
  }
  
/* showBasicUrl used for testing only */
function showBasicURL() {
    var safe = REVERSE_POST;
    document.getElementById('basicSampleUrl').innerHTML = safe.replace(/</g, '&lt;').replace(/>/g, '&gt;');
};

function doClick() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    var tempURL = REVERSE_POST.replace('YOUR_KEY_HERE', APP_KEY);
    var tempaURL = tempURL.replace('LAT', strlat);
    var newURL = tempaURL.replace('LNG', strlng);
    script.src = newURL;
    document.body.appendChild(script);
};

function renderGeocode(response) {
	if(response)
	var location = response.results[0].locations[0];
      var form1 = document.dealerSearchForm;
      if(form1.elements["zipCode"])
    	  form1.elements["zipCode"].value= location.postalCode;
      if(form1.elements["latitude"])
    	  form1.elements["latitude"].value= location.latLng.lat;
      if(form1.elements["longitude"])
    	  form1.elements["longitude"].value= location.latLng.lng;
      if(form1.elements["origaddress"])
    	  form1.elements["origaddress"].value= location.street;
      if(form1.elements["origcity"])
    	  form1.elements["origcity"].value= location.adminArea5;
      if(form1.elements["origstateprovince"])
    	  form1.elements["origstateprovince"].value= location.adminArea3;
      if(form1.elements["origPostal"])
    	  form1.elements["origPostal"].value= location.postalCode;

          	
};


function ajaxGeocodeCall(validate) 
{
	var form1 = document.dealerSearchForm;
	if(validate){
		
		if(!validateMapAddress(form1)){
			alert('Please enter a city and state');
			return false;
		}
	}
    var address = '';
    if(form1.elements["origaddress"]&& !form1.elements["origaddress"].value=="")
  	  address += form1.elements["origaddress"].value;
    if(form1.elements["origcity"]&& !form1.elements["origcity"].value=="")
    	 address += ','+form1.elements["origcity"].value;
    if(form1.elements["origstateprovince"]&& !form1.elements["origstateprovince"].value=="" ){
    	var e = document.getElementById("origstateprovince");
    	var state = e.options[e.selectedIndex].value;
    	address += ','+state;
    	address += ','+ form1.elements["origcountry"].value;
    }
    if(form1.elements["origPostal"]&& !form1.elements["origPostal"].value=="")
    	 address += ','+form1.elements["origPostal"].value;
    if(form1.elements["zipCode"]&& !form1.elements["zipCode"].value=="")
    		address += form1.elements["zipCode"].value;
	if(address==''){
		if(validate){
			document.dealerSearchForm.origcity.focus();
		}else{
			document.dealerSearchForm.zipCode.focus();
		}
		
		}else{		
			var tempURL = AJAX_POST.replace('YOUR_KEY_HERE', APP_KEY);
			var newURL = tempURL.replace('STREET', address);
		
			$.ajax({
				url: newURL,
				type: 'POST', // GET or POST
				dataType: 'jsonp', //return data type
				cache: false,
				timeout: 15000, //might need to bump up in dev environment
				error: function(){
					updateError();
				},
				success: function(json){
					updateGeocode(json);
				}
			});
		}
};
function getGeocode(validate) {
	var form1 = document.dealerSearchForm;
	if(validate){
		
		if(!validateMapAddress(form1)){
			alert('Please enter a city and state');
			return false;
		}
	}
    var address = '';
    if(form1.elements["origaddress"]&& !form1.elements["origaddress"].value=="")
  	  address += form1.elements["origaddress"].value;
    if(form1.elements["origcity"]&& !form1.elements["origcity"].value=="")
    	 address += ','+form1.elements["origcity"].value;
    if(form1.elements["origstateprovince"]&& !form1.elements["origstateprovince"].value=="" ){
    	var e = document.getElementById("origstateprovince");
    	var state = e.options[e.selectedIndex].value;
    	address += ','+state;
    	address += ','+ form1.elements["origcountry"].value;
    }
    if(form1.elements["origPostal"]&& !form1.elements["origPostal"].value=="")
    	 address += ','+form1.elements["origPostal"].value;
    if(form1.elements["zipCode"]&& !form1.elements["zipCode"].value=="")
    		address += form1.elements["zipCode"].value;
	if(address==''){
		if(validate){
			document.dealerSearchForm.origcity.focus();
		}else{
			document.dealerSearchForm.zipCode.focus();
		}

	}else{	
	
    var script = document.createElement('script');
    script.type = 'text/javascript';
    var tempURL = ADDRESS_POST.replace('YOUR_KEY_HERE', APP_KEY);

    var newURL = tempURL.replace('STREET', address);
    script.src = newURL;
    document.body.appendChild(script);
	}
}; 
function updateGeocode(response) {

	var location = response.results[0].locations[0];
    var form1 = document.dealerSearchForm;    
    if(form1.elements["latitude"])
	  form1.elements["latitude"].value= location.latLng.lat;
    if(form1.elements["longitude"])
	  form1.elements["longitude"].value= location.latLng.lng;
    refreshDealerDisplay();
  }

function updateError(){
	var form1 = document.dealerSearchForm;  
	form1.elements("zipCode").value = "";
	refreshDealerDisplay();
}
function doSidebarForm(){
	var form1 = document.sidebarLocatorForm;
    var address = '';
    if(form1.elements["zipCode"])
    		address += form1.elements["zipCode"].value;
	if(address==''){
		document.sidebarLocatorForm.zipCode.focus();
	}else{	
	
    var script = document.createElement('script');
    script.type = 'text/javascript';
    var tempURL = SIDEBAR_POST.replace('YOUR_KEY_HERE', APP_KEY);
    var newURL = tempURL.replace('STREET', address);
    script.src = newURL;
    document.body.appendChild(script);
	}
}; 
	
function updateSidebar(response) {

	var location = response.results[0].locations[0];
    var form1 = document.sidebarLocatorForm;    
    if(form1.elements["latitude"])
	  form1.elements["latitude"].value= location.latLng.lat;
    if(form1.elements["longitude"])
	  form1.elements["longitude"].value= location.latLng.lng;
    form1.submit();
  }	
var baseUrl = '';
function doCanadaParts() {
	var form1 = document.ZipSearchForm;

    var address = '';

    if(form1.elements["zipCode"]&& !form1.elements["zipCode"].value=="")
    		address += form1.elements["zipCode"].value;
	if(address==''){
			document.ZipSearchForm.zipCode.focus();
	}else{	
	
    var script = document.createElement('script');
    script.type = 'text/javascript';
    var tempURL = CAN_PARTS_POST.replace('YOUR_KEY_HERE', APP_KEY);

    var newURL = tempURL.replace('STREET', address);
    script.src = newURL;
    document.body.appendChild(script);
	}
}; 

function submitCanadaParts(response) {

	var location = response.results[0].locations[0];
    var form1 = document.ZipSearchForm;    
	  var latitude = location.latLng.lat;
	  var longitude = location.latLng.lng;
	  $(ZipSearchForm.productCategory).value = '';
	  form1.productCategory.value='';
	  $("input[name=productCategory]").value = '';
	  form1.action = form1.action + '&latitude=' + latitude + '&longitude='+ longitude + '&brandCode=6';
//    var formSubmitUrl = 'WhereToBuyDetailsView' + '?storeId=<c:out value="${WCParam.storeId}" escapeXml="true" />&catalogId=<c:out value="${WCParam.catalogId}" escapeXml="true" />&langId=<c:out value="${WCParam.langId}" escapeXml="true" />';
//    formSubmitUrl += '&latitude=' + latitude + '&longitude='+ longitude + '&searchType=4&searchField=ZIPCODE&proximity=100&brandCode=6';
//    formSubmitUrl +='&zipCode=' + form1.zipCode.value
//    window.location.href =formSubmitUrl;
	  form1.productCategory
    form1.submit();
  }	

function loadPoiMap(txt, urlRoot, strlat, strlng){
    var temp =  txt.replace(/&#034;/gi,'\"');
    var data =  temp.replace(/&#039;/gi,'\'');
    var mqResponse =JSON.parse(data);
    var searchResults = mqResponse.searchResults;

     var pois = new MQA.ShapeCollection();
 

     /*Add POI markers and populate the search result table*/ 
     /* build start location */


     	var custom=new MQA.Poi( {lat:strlat, lng:strlng} );
     	var icon =new MQA.Icon("http://developer.mapquest.com/content/documentation/common/images/smiley.png",32,52);

     	  custom.setIcon(icon);
     	  pois.add(custom);
     for (i=0;i<searchResults.length;i++) {
       var location = new MQA.Poi({lat:searchResults[i].shapePoints[0],lng:searchResults[i].shapePoints[1]});

       /*Change default POI icons to numbered icons*/ 
       var numberedPoi = new MQA.Icon('http://www.mapquestapi.com/staticmap/geticon?uri=poi-' + (i+1) + '.png',20,29);
       location.setIcon(numberedPoi);

       /*Populate the content within the InfoWindows*/ 
       var locName= searchResults[i].name;
       var locAddress1 = searchResults[i].fields.address;
       var locAddress2 = searchResults[i].fields.UserField03;
       var locCity = searchResults[i].fields.city;
       var locState = searchResults[i].fields.state;
       var locPostal = searchResults[i].fields.postal;
       var locPhone = searchResults[i].fields.UserField02;
       var locDistance = Math.round(searchResults[i].distance*10)/10
       var locLat= searchResults[i].fields.Lat;
       var locLng= searchResults[i].fields.Lng;
       
       var urlString = urlRoot;
		urlString += '&map=true&destname=' + locName ;
		urlString += '&destaddress=' + locAddress1;
		urlString += '&destcity=' + locCity;
		urlString += '&deststateprovince=' +locState ;	
		urlString += '&destpostalcode=' + locPostal;
		urlString += '&destphone=' +locPhone ;
		urlString += '&destlat=' +locLat ;
		urlString += '&destlng=' +locLng ;


		location.setRolloverContent('<div style="width:200px; font:bold 14px Helvetica, Arial, sans-serif;">' + searchResults[i].name + '</div>');
       location.setInfoContentHTML('<div style="width:200px; font:bold 14px Helvetica, Arial, sans-serif;"><a href="' + urlString + '" escape="true">' + searchResults[i].name + '</a></div>' + searchResults[i].fields.address + '<br />' + searchResults[i].fields.city + ', OH ' + searchResults[i].fields.postal);

       pois.add(location);
		
     }


     /*Create an object for options*/ 
     var options={
       elt:document.getElementById('mapPoi'),       /*ID of element on the page where you want the map added*/ 
       collection:pois                           /*initialize map with shape collection*/ 
     };

     /*Construct an instance of MQA.TileMap with the options object*/ 
     window.map = new MQA.TileMap(options);

     MQA.withModule('largezoom', 'viewoptions', 'traffictoggle', 'mousewheel', 'shapes', function() {
       map.addControl(new MQA.LargeZoom(), new MQA.MapCornerPlacement(MQA.MapCorner.TOP_LEFT, new MQA.Size(5,5)));
       map.addControl(new MQA.TrafficToggle());
       map.addControl(new MQA.ViewOptions());
       map.enableMouseWheelZoom();

     });
    
}
function loadMap(destlat, destlng){     
    var options={
      elt:document.getElementById('map'),       /*ID of element on the page where you want the map added*/ 
      zoom:10,                                  /*initial zoom level of the map*/ 
      latLng:{lat:destlat, lng:destlng},  /*center of map in latitude/longitude */ 
      mtype:'map',                              /*map type (map)*/ 
      bestFitMargin:0,                          /*margin offset from the map viewport when applying a bestfit on shapes*/ 
      zoomOnDoubleClick:true                    /*zoom in when double-clicking on map*/ 
    };
    /*Construct an instance of MQA.TileMap with the options object*/ 
    window.map = new MQA.TileMap(options);
    var basic=new MQA.Poi( {lat:destlat, lng:destlng} );
	  /*This will add the POI to the map in the map's default shape collection.*/ 
	  window.map.addShape(basic);
	  
    MQA.withModule('largezoom','traffictoggle','viewoptions','geolocationcontrol','insetmapcontrol','mousewheel', function() {	
  	window.map.addControl(
    		new MQA.LargeZoom(),
    		new MQA.MapCornerPlacement(MQA.MapCorner.TOP_LEFT, new MQA.Size(5,5))
  	);
		window.map.addControl(
          new MQA.GeolocationControl(),
          new MQA.MapCornerPlacement(MQA.MapCorner.TOP_RIGHT, new MQA.Size(10,50))
      );

		});
 };
 var driveUnits='m';
 var distUnits='mi';
 var strUnits=' miles ';
 var strEstimated = ' Estimated driving time ';
 var strDistance = ' Distance ';
 var strMap = ' Map ';
 var strDirections = ' Driving Directions ';
 
function loadDirectionsMap(latitude, longitude, destlat, destlng, localeLang){
	
	if(localeLang != 'en_US'){
		driveUnits='k';
		distUnits = 'km';
		strUnits = ' kilometre ';
		if(localeLang == 'fr_FR'){
			strUnits = ' kilom&#232;tre ';
			strEstimated = ' Temps de conduite estim&#233;e ';
			strDistance = ' Distance ';
			strMap = ' Carte ';
			strDirections = ' Itin&#233;raire ';
		}
	}
		
    /*Create an object for options*/ 
   var options={
     elt:document.getElementById('routeMap'),       /*ID of element on the page where you want the map added*/ 
     zoom:10,                                  /*initial zoom level of the map*/ 
     latLng:{lat:latitude, lng:longitude},  /*center of map in latitude/longitude */ 
     mtype:'map',                            /*map type (map)*/ 
     unit:driveUnits   

   };

   /*Construct an instance of MQA.TileMap with the options object*/ 
   window.map = new MQA.TileMap(options);

MQA.withModule('directions', function() {
	
 /*Uses the MQA.TileMap.addRoute function (added to the TileMap with the directions module) 
 passing in an array of location objects as the only parameter.*/ 
 window.map.addRoute([
   {latLng: {lat:latitude, lng:longitude}},
   {latLng: {lat:destlat, lng:destlng}}
 ],
    /*Add options.*/ 
   {routeOptions:{locale:localeLang,unit:driveUnits},	 
    ribbonOptions:{draggable:true}},

   /*Add the callback function to the route call.*/ 
   displayNarrative);

});
 /*Add the zoom control and locator icon.*/ 
 MQA.withModule('largezoom','traffictoggle','viewoptions','geolocationcontrol','insetmapcontrol','mousewheel', function() {
	
 window.map.addControl(
   new MQA.LargeZoom(),
   new MQA.MapCornerPlacement(MQA.MapCorner.TOP_LEFT, new MQA.Size(5,5))
 );
	window.map.addControl(
         new MQA.GeolocationControl(),
         new MQA.MapCornerPlacement(MQA.MapCorner.TOP_RIGHT, new MQA.Size(10,50))
       );
       });
}

/*Build the detaild directions table.*/ 
function displayNarrative(data){
 if(data.route){
   var legs = data.route.legs, html = '', i = 0, j = 0, trek, maneuver;
   html +='<table class="mqTable" ><tr><td colspan="2"><b>'	;
	  html += strEstimated + data.route.formattedTime;
	  html +='</b></td><td colspan="2">';
	  html += '<b>'+ strDistance + data.route.distance.toFixed(2);
	  html += ' ' +  strUnits + '</b></td></tr>';
	  html += '<td colspan="4" class="mqHline"><img src="/wcpics/global/images/map/images/t.gif" width="1" height="1"></td>'; 
	  
   html += '<tr><th>'+strMap+'</th><th colspan=2>'+ strDirections + '</th><th>'+ strDistance+ '</th><tbody>';
    
   for (; i < legs.length; i++) {
	
     for (j = 0; j < legs[i].maneuvers.length; j++) {
      html += '<td colspan="4"  class="mqHline"><img src="/wcpics/global/images/map/images/t.gif" width="1" height="1"></td>';
       maneuver = legs[i].maneuvers[j];
       html += '<tr>';
       if(maneuver.mapUrl) {
        		html += '<td><img src="' + maneuver.mapUrl + '"></td>  ';	

		  }else{
				html +='<td></td>';
		  }
		  html += '<td>';	
       if (maneuver.iconUrl) {
         html += '<img src="' + maneuver.iconUrl + '">  ';
       }

       for (k = 0; k < maneuver.signs.length; k++) {
         var sign = maneuver.signs[k];
         if (sign && sign.url) {
           html += '<img src="' + sign.url + '">  ';
         }
       }
		 
       html += '</td><td>' + maneuver.narrative + '</td>';
       html += '</td><td>' + maneuver.distance.toFixed(2) + ' '+distUnits + '</td>'; 
       html += '</tr>';
     }
   }
   
   html += '</tbody></table>';
   document.getElementById('routeLegs').innerHTML = html;
 }
}
//The following functions taken from formval.js - no import so as to not override other functions
function validateMapAddress(form){
	var errs = 0;
	if (!validateMapField(form.origcity)) errs += 1;
	if (!validateMapField(form.origstateprovince)) errs += 1;
	
	if (errs > 0){
		return false;
		
	}
	else {
		return true;
	}
}

		
function validateMapField (field1){
	field1.value = field1.value.replace(/^\s+|\s+$/g, '');
	if (field1.value == ''){
		return false;
	}			
	else {
		return true;
	}
}
