# MTD Products Canada

## Template Preview Links:

- [Home](http://mtdproducts-ca.herokuapp.com)
- [Our Company](http://mtdproducts-ca.herokuapp.com/our-company)
- [Our Products](http://mtdproducts-ca.herokuapp.com/our-products)
- [Our Caring](http://mtdproducts-ca.herokuapp.com/our-caring)
- [Products Listing](http://mtdproducts-ca.herokuapp.com/our-products/list)
- [Products Detail](http://mtdproducts-ca.herokuapp.com/our-products/detail)
- [Search Results](http://mtdproducts-ca.herokuapp.com/search)
- [FAQs](http://mtdproducts-ca.herokuapp.com/faqs)
- [Parts Landing](http://mtdproducts-ca.herokuapp.com/parts)
- [Independent Dealers Search](http://mtdproducts-ca.herokuapp.com/dealers)
- [Independent Dealers Results](http://mtdproducts-ca.herokuapp.com/dealers/results)
- [Independent Dealers Directions](http://mtdproducts-ca.herokuapp.com/dealers/directions?langId=-1&map=true&destname=CUTTING+EDGE+LAWN+CENTER&destaddress=603+SOUTH+SANTA+FE+&destcity=VISTA&deststateprovince=CA&destpostalcode=92084&destphone=(760)+726-3931&destlat=33.196126&destlng=-117.238360&latitude=33.083&longitude=-117.23696&origcountry=US)
- [Independent Dealers Directions Results](http://mtdproducts-ca.herokuapp.com//dealers/directions/results?storeId=10500&catalogId=20500&map=false&transaction=route&template=route&routeMaps=3&mapWidth=525&destname=CUTTING+EDGE+LAWN+CENTER&destaddress=603+SOUTH+SANTA+FE+&destcity=VISTA&deststateprovince=CA&destpostalcode=92084&destphone=%28760%29+726-3931&destlat=33.196126&destlng=-117.238360&latitude=33.083&longitude=-117.23696&origcountry=US&origaddress=7581+Dehesa+Ct&origcity=Carlsbad&origstateprovince=CA)
- [French Site](http://mtdproducts-ca.herokuapp.com/?language=fr)
- [Product Registration](http://mtdproducts-ca.herokuapp.com/product-registration)
- [Product Registration Survey](http://mtdproducts-ca.herokuapp.com/product-registration/survey)

## Task List

### Setup 

* Pull down current mtdproducts.com site CSS, JS files and any images needed.
* Create layout template (site header and footer)

### Global  Site Header

* Remove global site link and careers link
* Add link to toggle French & English versions

### Global Navigation (Our Company tab)

* Our Company (About MTD Canada, Contact Us)
* Shop by brand (MTD, Bolens, White Outdoor, Yard-Man, Yard Machines, Remington, Columbia)
  
### Global Navigation (Our Products tab)

* Lawn & Garden (Lawn Mowers & Tractors, Zero-Turn Riders, Trimmers, Tillers and Cultivators)
* Trees & Leaves (Leaf Blowers, Chipper Shredders & Vacs, Log Splitters, Chainsaws)
* Snow (Snow Throwers)
* Shop by brand (MTD, Bolens, White Outdoor, Yard-Man, Yard Machines, Remington, Columbia)
  
### Global Navigation (Our Caring tab)

* Environmental (The Benefits of Turfgrass)
* Safety (Lawn Mower Safety, Snow Thrower Safety)
  
### Global Navigation (Owners Centre tab)

* Owners Centre (Product Manuals, Product Registration, Replacement Parts Accessories & Attachments, Find a Service Centre, FAQs, Tips & Tricks, Diagnose & Troubleshoot, Maintenance, Contact Us)
* Register a Product (Get Product Notifications, Easily Find Replacement Parts, Other Great Benefits)
* Find a Part (Parts), only a link - remove form

### Global Navigation (Parts tab)

* Implement new design

### Site Footer

* Remove CA Supply Chains Act

### Homepage  

* Brand bar logos (MTD, Bolens, White Outdoor, Yard-Man, Yard Machines, Remington, Columbia)

### Our Company Landing 

* Update left nav, removing Careers link

### Our Products Landing  

* Update left nav to match global nav contents
* Add Parts link to left nav - link goes to new generic Canadian parts page
* Shop by brand section - update to match logos that are applicable to Canada


### Our Caring Landing  

* Environmental section remove “The TurfMutt Education Program”

### Owners Centre Landing 

* Update FAQ layout with new design

### Parts Landing 

* New template

### Product Listing 

* New link for Parts in left nav, replacing Part Finder form on .com
* Add espot in left nav
* Remove Sort by
* Make Product title a link that goes to new Product detail template
* Remove price from product listing

### Product Detail  

* New template

### Search Results Listing  

* Remove Sort By
* Make Product title a link that goes to new Product detail template
* Remove price from product listing

### Independent Dealers 

* New template for search form (use Find a Service Center as guide)
* New template for search results (use Find a Service Center as guide)
* New template for directions form (use Find a Service Center as guide)

### French translation tasks  

* will need Parts nav translation in French
* other possible needs
